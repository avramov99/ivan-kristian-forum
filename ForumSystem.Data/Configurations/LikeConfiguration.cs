﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ForumSystem.Models.EntityModels;

namespace ForumSystem.Data.Configurations
{
    public class LikeConfiguration : IEntityTypeConfiguration<Like>
    {
        public void Configure(EntityTypeBuilder<Like> like)
        {
            like
                .HasKey(l => new { l.PostId, l.UserId });
        }
    }
}
