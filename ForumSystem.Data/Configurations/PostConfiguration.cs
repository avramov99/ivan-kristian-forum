﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ForumSystem.Models.EntityModels;

namespace ForumSystem.Data.Configurations
{
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> post)
        {
            post
                .HasOne(p => p.Author)
                .WithMany(a => a.Posts)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
