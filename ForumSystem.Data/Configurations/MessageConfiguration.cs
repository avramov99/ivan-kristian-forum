﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ForumSystem.Models.EntityModels;

namespace ForumSystem.Data.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> message)
        {
            message
                .HasOne(m => m.Author)
                .WithMany(a => a.SentMessages)
                .OnDelete(DeleteBehavior.NoAction);

            message
                .HasOne(m => m.Receiver)
                .WithMany(r => r.ReceivedMessages)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
