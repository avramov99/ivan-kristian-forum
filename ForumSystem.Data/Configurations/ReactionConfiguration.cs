﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ForumSystem.Models.EntityModels;

namespace ForumSystem.Data.Configurations
{
    public class ReactionConfiguration : IEntityTypeConfiguration<Reaction>
    {
        public void Configure(EntityTypeBuilder<Reaction> reaction)
        {
            reaction
                .HasKey(r => new { r.CommentId, r.UserId });
        }
    }
}
