﻿using Microsoft.EntityFrameworkCore;

using ForumSystem.Models.EntityModels;

namespace ForumSystem.Data
{
    public class ForumSystemContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Post> Posts { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        public virtual DbSet<Like> Likes { get; set; }

        public virtual DbSet<Reaction> Reactions { get; set; }

        public virtual DbSet<Message> Messages { get; set; }

        public virtual DbSet<ProfilePicture> ProfilePictures { get; set; }



        private readonly string connectionString = @"Server=.\SQLEXPRESS;Database=ForumSystemDatabase;Integrated Security=True;";



        public ForumSystemContext()
        {
        }

        public ForumSystemContext(DbContextOptions<ForumSystemContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }
    }
}
