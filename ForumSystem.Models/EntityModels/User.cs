﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common;
using ForumSystem.Models.Common.Contracts;
using ForumSystem.Models.Common.Enums;

namespace ForumSystem.Models.EntityModels
{
    public class User : IAuditInfo, IDeletableEntity
    {
        [Key]
        public long UserId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ErrorMessages.UserUsernameLengthErrorMessage)]
        public string Username { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 8, ErrorMessage = ErrorMessages.UserPasswordLengthErrorMessage)]
        [RegularExpression(@"^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%?&])[A-Za-z\d@$!%?&]{8,32}$",
            ErrorMessage = ErrorMessages.UserInvalidPasswordErrorMessage)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ErrorMessages.UserDisplayNameLengthErrorMessage)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(320, MinimumLength = 3, ErrorMessage = ErrorMessages.UserEmailAddressLengthErrorMessage)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = ErrorMessages.UserInvalidEmailAddressErrorMessage)]
        public string Email { get; set; }

        [Required]
        public RoleType Role { get; set; }

        [ForeignKey("ProfilePicture")]
        public long ProfilePictureId { get; set; }
        public ProfilePicture ProfilePicture { get; set; }

        [StringLength(250, MinimumLength = 3, ErrorMessage = ErrorMessages.UserBiographyLengthErrorMessage)]
        public string Biography { get; set; }

        public GenderType Gender { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ICollection<Post> Posts { get; set; } = new HashSet<Post>();

        public virtual ICollection<Like> LikedPosts { get; set; } = new HashSet<Like>();

        public virtual ICollection<Comment> Comments { get; set; } = new HashSet<Comment>();

        public virtual ICollection<Reaction> CommentReactions { get; set; } = new HashSet<Reaction>();

        public virtual ICollection<Message> SentMessages { get; set; } = new HashSet<Message>();

        public virtual ICollection<Message> ReceivedMessages { get; set; } = new HashSet<Message>();

        // public virtual ICollection<Follower> Followers { get; set; } = new HashSet<Follower>();
    }
}
