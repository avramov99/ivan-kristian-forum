﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ForumSystem.Models.EntityModels
{
    public class ProfilePicture
    {
        [Key]
        public long ProfilePictureId { get; set; }

        [Required]
        public byte[] Image { get; set; }
    }
}
