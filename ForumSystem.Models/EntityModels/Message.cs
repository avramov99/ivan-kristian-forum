﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common;

namespace ForumSystem.Models.EntityModels
{
    public class Message
    {
        [Key]
        public long MessageId { get; set; }

        [Required]
        [StringLength(300, MinimumLength = 1, ErrorMessage = ErrorMessages.MessageContentLengthErrorMessage)]
        public string Content { get; set; }

        [Required]
        [ForeignKey("Author")]
        public long AuthorId { get; set; }
        public User Author { get; set; }

        [Required]
        [ForeignKey("Receiver")]
        public long ReceiverId { get; set; }
        public User Receiver { get; set; }
    }
}
