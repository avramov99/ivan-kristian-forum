﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common;
using ForumSystem.Models.Common.Contracts;

namespace ForumSystem.Models.EntityModels
{
    public class Post : IAuditInfo, IDeletableEntity
    {
        [Key]
        public long PostId { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = ErrorMessages.PostTitleLengthErrorMessage)]
        public string Title { get; set; }

        [Required]                         
        [StringLength(30000, MinimumLength = 3, ErrorMessage = ErrorMessages.PostDescriptionLengthErrorMessage)]
        public string Description { get; set; }

        [Required]
        [ForeignKey("Author")]
        public long AuthorId { get; set; }
        public User Author { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ICollection<Like> Likes { get; set; } = new HashSet<Like>();

        public virtual ICollection<Comment> Comments { get; set; } = new HashSet<Comment>();
    }
}
