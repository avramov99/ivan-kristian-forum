﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common.Contracts;
using ForumSystem.Models.Common.Enums;

namespace ForumSystem.Models.EntityModels
{
    public class Reaction : IAuditInfo
    {
        [Required]
        public ReactionType ReactionType { get; set; }

        [Required]
        [ForeignKey("Comment")]
        public long CommentId { get; set; }
        public Comment Comment { get; set; }

        [Required]
        [ForeignKey("User")]
        public long UserId { get; set; }
        public User User { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
