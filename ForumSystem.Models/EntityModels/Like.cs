﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common.Contracts;

namespace ForumSystem.Models.EntityModels
{
    public class Like : IAuditInfo
    {
        [Required]
        [ForeignKey("Post")]
        public long PostId { get; set; }
        public Post Post { get; set; }

        [Required]
        [ForeignKey("User")]
        public long UserId { get; set; }
        public User User { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
