﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using ForumSystem.Models.Common;
using ForumSystem.Models.Common.Contracts;

namespace ForumSystem.Models.EntityModels
{
    public class Comment : IAuditInfo, IDeletableEntity
    {
        [Key]
        public long CommentId { get; set; }

        [Required]
        [StringLength(3000, MinimumLength = 3, ErrorMessage = ErrorMessages.CommentDescriptionLengthErrorMessage)]
        public string Description { get; set; }

        [Required]
        [ForeignKey("Post")]
        public long PostId { get; set; }
        public Post Post { get; set; }

        [Required]
        [ForeignKey("Author")]
        public long AuthorId { get; set; }
        public User Author { get; set; }

        [ForeignKey("Parent")]
        public long? ParentId { get; set; }
        public Comment Parent { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ICollection<Reaction> Reactions { get; set; } = new HashSet<Reaction>();
    }
}
