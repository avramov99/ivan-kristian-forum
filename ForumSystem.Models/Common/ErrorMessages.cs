﻿namespace ForumSystem.Models.Common
{
    public class ErrorMessages
    {
        // User
        public const string UserUsernameLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string UserPasswordLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string UserEmailAddressLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string UserDisplayNameLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string UserBiographyLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string UserInvalidPasswordErrorMessage = "Invalid password.";
        public const string UserInvalidEmailAddressErrorMessage = "Invalid email address.";

        // Post
        public const string PostTitleLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
        public const string PostDescriptionLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";

        // Comment
        public const string CommentDescriptionLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";

        // Message
        public const string MessageContentLengthErrorMessage = "The {0} must be at least {2} and at max {1} characters long.";
    }
}
