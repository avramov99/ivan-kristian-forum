﻿namespace ForumSystem.Models.Common.Enums
{
    public enum ReactionType : byte
    {
        Like = 0,
        Love = 1,
        Haha = 2,
        Wow = 3,
        Sad = 4,
        Angry = 5
    }
}
