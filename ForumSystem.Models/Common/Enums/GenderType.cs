﻿namespace ForumSystem.Models.Common.Enums
{
    public enum GenderType : byte
    {
        Male = 0, 
        Female = 1, 
        Other = 2
    }
}
