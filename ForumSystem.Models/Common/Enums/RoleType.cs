﻿namespace ForumSystem.Models.Common.Enums
{
    public enum RoleType : byte
    {
        Standard = 0, 
        Admin = 1
    }
}
